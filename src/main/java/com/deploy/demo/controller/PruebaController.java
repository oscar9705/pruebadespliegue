package com.deploy.demo.controller;

import com.deploy.demo.service.PruebaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prueba")
public class PruebaController {
    private final PruebaService pruebaService;
    @Autowired
    public PruebaController(PruebaService pruebaService) {
        this.pruebaService = pruebaService;
    }

    @GetMapping(path = "/all")
    public String sayHelloWorld() {
        return pruebaService.sayHelloWorld();
    }
}

