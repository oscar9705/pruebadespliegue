package com.deploy.demo;

import com.deploy.demo.controller.PruebaController;
import com.deploy.demo.service.PruebaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DeployApplicationTests {
    @Autowired
    private final PruebaService pruebaService = new PruebaService();



    @Test
    void sayHelloWorld(){
        String p = pruebaService.sayHelloWorld();
        assertEquals("Hello Deploy in aws",p);
    }


}
