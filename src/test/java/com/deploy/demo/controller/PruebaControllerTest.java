package com.deploy.demo.controller;

import com.deploy.demo.service.PruebaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebaControllerTest {

    @Mock
    private PruebaService pruebaServ;

    @InjectMocks
    private PruebaController pruebaController;

    @BeforeEach
    void init(){
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void pruebaControl(){
        Mockito.when(pruebaServ.sayHelloWorld()).thenReturn("Hello Deploy in aws");
        String p= pruebaController.sayHelloWorld();
        assertEquals("Hello Deploy in aws",p);
    }
}
